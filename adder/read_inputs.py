# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 09:22:23 2022

@author: jfrankem
"""

import pandas as pd
import numpy as np
import os
import urllib.request as rq
from abc import ABC, abstractmethod

class Meteo:
    # The Meteo class mimics the PyCMD Meteo class, and can be used when the former is not available.
    def __init__(self,U,wd,E,Ta,t,height,T):
        
        TypeError1 = TypeError('U, wd, E, Ta, and t must be equal-length and of type np.ndarray with ndim=1.')
        TypeError2 = TypeError('height must be of type float.')
        if isinstance(U,np.ndarray) and isinstance(wd,np.ndarray) and isinstance(E,np.ndarray) and isinstance(Ta,np.ndarray) and isinstance(t,np.ndarray):
            if U.ndim == 1 and wd.ndim == 1 and E.ndim == 1 and Ta.ndim == 1 and t.ndim == 1:
                pass
            else:
                raise TypeError1
        else:
            raise TypeError1
        
        if len(U) == len(wd) and len(U) == len(E) and len(U) == len(Ta) and len(U) == len(t):
            pass
        else:
            raise TypeError1
        
        if not isinstance(height,float):
            raise TypeError2
        
        self.U = U
        self.wd = wd # wind direction
        self.E = E # stability class
        self.Ta = Ta #ambient temperature
        self.t = t # time
        self.T = T #measurement interval
        
        self.height = height # measurement height

class Source:
    # The Source class mimicks the PyCMD SourceBR1RV class, and can be used in its place when not available.   
    def __init__(self,Q,t,Hs,Vs=None,Ts=None):
        TypeError1 = TypeError('Q and t must be equal-length and of type np.ndarray with ndim=1.')
        TypeError2 = TypeError('Hs, Vs, and Ts must be of type float.')
        if isinstance(Q,np.ndarray) and isinstance(t,np.ndarray):
            if Q.ndim == 1 and t.ndim == 1:
                pass
            else:
                raise TypeError1
        else:
            raise TypeError1
        
        if not len(Q) == len(t):
            raise TypeError1
        if not isinstance(Hs,float):
            raise TypeError2
        if not isinstance(Ts,float):
            raise TypeError2
        if not isinstance(Vs,float):
            raise TypeError2
        
        self.Q = Q
        self.t = t
        self.Hs = Hs
        self.Vs = Vs
        self.Ts = Ts


class data_collection(object):
    pass

def instance_of_data_collection():
    instance = data_collection()
    return instance

def read_lara(nuclide):
    """
    
    Description
    -----------
    This function processes information sheets from lara on the Web into usable
    numpy arrays. Download the nuclide of interest from Lara on the Web and put
    it into the data/nuclides folder. Right now, it only picks out the gammas.
    
    NB. Turn daughter nuclides off.

    Parameters
    ----------
    nuclide : name of nuclide in the form 'Se-75'
    
    Returns
    -------
    nuclide_data : collection of gamma energies [KeV] and intensities [0-1]
    
    References
    ----------
    http://www.nucleide.org/Laraweb/index.php
    
    """
    
    nuclide_data = instance_of_data_collection()
    
    absolute_path      = os.path.dirname(__file__)
    relative_directory = 'data/nuclides'
    directory = os.path.join(absolute_path,relative_directory)
    filenames = os.listdir(directory)
    filepaths = [os.path.join(directory, f) for f in filenames]
    for i in range(len(filenames)):
        if nuclide in filenames[i]:
            if not(nuclide[len(nuclide)-1] != 'm' and filenames[i][len(nuclide)] == 'm'):
                filepath = filepaths[i]            
    try:
        file = open(filepath)
    except:
        print('This nuclide is not yet included in data/nuclides. Go to '
              'http://www.nucleide.org/Laraweb/index.php and download '
              'the data and emissions file in ASCII text format, e.g.'
              'Se-75.lara.txt. Put it in the folder and rerun.')
    
    linenum = -1
    for line in file:
        linenum += 1
        if 'Nuclide' in line:
            nuclide_data.nuclide = line[line.find(' ; ')+3:-1].strip()
        elif 'Element' in line:
            nuclide_data.element = line[line.find(' ; ')+3:-1].strip()
        elif '----------' in line:
            linenum_start = linenum
    file.close()
        
    data = pd.read_csv(os.path.join(absolute_path,filepath),header=linenum_start+1,engine='python',sep=' ; ',index_col=False,usecols=range(5))
    data.drop(data.tail(1).index,inplace=True)
    data = data[data['Type']=='g']
    
    nuclide_data.Ey=np.squeeze(pd.DataFrame(data,columns = ['Energy (keV)']).to_numpy(dtype=np.float64))
    nuclide_data.I=np.squeeze(pd.DataFrame(data,columns = ['Intensity (%)']).to_numpy())/100 # to fractions
    
    return nuclide_data

def read_livechart_api(nuclide,E_c=100,I_c=0.05):
    
    nuclide_data = instance_of_data_collection()
    
    # the service URL
    livechart = "https://nds.iaea.org/relnsd/v1/data?"
    
    def lc_pd_dataframe(url):
        req = rq.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0')
        return pd.read_csv(rq.urlopen(req))
    
    df = lc_pd_dataframe(livechart + "fields=decay_rads&nuclides="+nuclide+"&rad_types=g")
    
    df = df[pd.to_numeric(df['intensity'],errors='coerce').notna()] # remove blanks (unknown intensities)
    df.intensity = df['intensity'].astype(float) # convert to numeric. Note how one can specify the field by attribute or by string 
    df = df.drop(df[df.intensity < I_c*100].index)
    df = df.drop(df[df.energy < E_c].index)
    
    nuclide_data.Ey= df['energy'].to_numpy(dtype=np.float64)
    nuclide_data.I=  df['intensity'].to_numpy(dtype=np.float64)/100 # to fractions
    
    return nuclide_data

def read_selenium_meteo(t0,t1):
    abspath     = os.path.dirname(__file__)
    relpath     = 'data/meteo/met20190515.txt'
    data        = pd.read_csv(os.path.join(abspath,relpath),sep=';')
    t           = pd.to_datetime(data['Date_Time'],format='%Y-%m-%d %H:%M:%S').to_numpy()
    meteo_window = (t>=t0) == (t<=t1)
    t           = pd.to_datetime(data['Date_Time'],format='%Y-%m-%d %H:%M:%S').to_numpy()[meteo_window]
    T8          = np.squeeze(pd.DataFrame(data,columns = ['T8']).to_numpy())[meteo_window]
    T114        = np.squeeze(pd.DataFrame(data,columns = ['T114']).to_numpy())[meteo_window]
    height      = 69.
    Ta          = T8 + (T114-T8)/(114-8)*(height-8)
    U           = np.squeeze(pd.DataFrame(data,columns = ['Speed']).to_numpy())[meteo_window]
    wd          = np.squeeze(pd.DataFrame(data,columns = ['Azimuth']).to_numpy())[meteo_window]
    sig_wd      = np.squeeze(pd.DataFrame(data,columns = ['AzimSigma']).to_numpy())[meteo_window]
    E           = np.squeeze(pd.DataFrame(data,columns = ['E_dT']).to_numpy())[meteo_window]
    T           = 10.
    return Meteo(U, wd, E, Ta, t, height, T)