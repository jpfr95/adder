# -*- coding: utf-8 -*-
"""
Created on Tue Mar  4 18:21:56 2025

@author: Jens Peter
"""

from abc import ABC, abstractmethod


class Deposition(ABC):
    
    @abstractmethod
    def calculate(self):
        pass

# these classes still need to be implemented

class DryDeposition(Deposition):
    
    def __init__(self):
        pass
    
    def calculate(self):
        pass
    
class WetDeposition(Deposition):
    
    def __init__(self):
        pass
    
    def calculate(self):
        pass
    
class TotalDeposition(Deposition):
    
    def __init__(self):
        pass
    
    def calculate(self):
        pass