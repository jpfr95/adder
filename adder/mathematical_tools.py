# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 10:53:37 2022

@author: jfrankem
"""

import numpy as np

class data_collection(object):
    pass

def instance_of_data_collection():
    instance = data_collection()
    return instance

class Grid:
    
    def __init__(self):
        self.X = None
        self.Y = None
        self.Z = None
        pass
    
    def rotate(self,wd):
        """
        Parameters
        ----------
        X : Meshgrid of longitudinal coordinates (Lambert 72) w.r.t. stack [m]
        Y : Meshgrid of longitudinal coordinates (Lambert 72) w.r.t. stack [m]
        wd: Wind direction (antiparallel to wind vector) [deg. w.r.t. North]
    
        Returns
        -------
        Xrot,Yrot : X and Y components of a meshgrid rotated by (wd-90)/180*np.pi
    
        """
        r = np.sqrt(self.X**2+self.Y**2)
        phi = np.arctan2(self.Y,self.X) - np.pi + (wd-90)/180*np.pi
        Xrot = r*np.cos(phi)
        Yrot = r*np.sin(phi)
        return Xrot,Yrot

class SquareGrid(Grid):
    
    def __init__(self,bx,by,bz,Nx,Ny,Nz):
                  
        self.bx, self.by, self.bz = bx, by, bz
        self.Nx, self.Ny, self.Nz = Nx, Ny, Nz
        
        self.initialise()

    def initialise(self):
        """
        Parameters
        ----------
        self.bx : FLOAT
            Grid boundary in x direction (-xb to xb)
        self.by : FLOAT
            Grid boundary in y direction (-yb to yb).
        self.bz : FLOAT
            Grid boundary in z direction (0 to zb).
        self.Nx : INT
            Number of cells in x direction.
        self.Ny : INT
            Number of cells in y direction.
        self.Nz : INT
            Number of cells in z direction.

        Returns (to object)
        -------
        self.X, self.Y, self.Z : MESHGRIDS
            X, Y and Z meshgrids
        self.dx, self.dy, self.dz: FLOAT
            Cell-to-cell distances
        """

        if self.Nx > 1:
            self.dx = 2*self.bx/(self.Nx-1)
            x = np.linspace(-self.bx,self.bx,self.Nx)
        else:
            self.dx = 1
            x = self.bx
        if self.Ny > 1:
            self.dy = 2*self.by/(self.Ny-1)
            y = np.linspace(-self.by,self.by,self.Ny)
        else:
            self.dy = 1
            y = self.by
        if self.Nz > 1:
            self.dz = self.bz/(self.Nz-1)
            z = np.linspace(0,self.bz,self.Nz)
        else:
            self.dz = 1
            z = self.bz

        self.X,self.Y,self.Z = np.meshgrid(x,y,z)

class UnstructuredGrid(Grid):

    def __init__(self,xlist,ylist,zlist):
        if isinstance(xlist, list) and isinstance(ylist, list) and isinstance(zlist, list):
            self.X = np.array([[xlist]])
            self.Y = np.array([[ylist]])
            self.Z = np.array([[zlist]])
        else:
            raise Exception('Nomen est omen: Xlist, Ylist and Zlist must be lists.')

class UniformGrid(Grid):
    
    def __init__(self,Nx,Ny,Nz,bx_west,by_south,bz_low,
                 bx_east=None,by_north=None,bz_high=None):
        self.bx_west,   self.bx_east    = bx_west,  bx_east
        self.by_south,  self.by_north   = by_south, by_north
        self.bz_low,    self.bz_high    = bz_low,   bz_high
        self.Nx,        self.Ny         = Nx,       Ny
        self.Nz                         = Nz
        
        if Nx == 1:
            self.dx = np.nan
            x = bx_west
        else:
            self.dx = (bx_east-bx_west)/(Nx-1)
            x = np.linspace(bx_west,bx_east,Nx)
            
        if Ny == 1:
            self.dx = np.nan
            y = by_south
        else:
            self.dy = (by_north-by_south)/(Ny-1)
            y = np.linspace(by_south,by_north,Ny)
            
        if Nz == 1:
            self.dz = np.nan
            z = bz_low
        else:
            self.dz = (bz_high-bz_low)/(Nz-1)
            z = np.linspace(bz_low,bz_high,Nz)

        self.X,self.Y,self.Z = np.meshgrid(x,y,z)

    def to_L72(self,x_ref,y_ref):
        self.lat, self.lon = self.Y, self.X
        self.X, self.Y = WGS84_to_L72(self.Y, self.X, x_ref, y_ref)
        self.dy = self.Y[1,0,0]-self.Y[0,0,0]
        self.dx = self.X[0,1,0]-self.X[0,0,0]

def create_square_centered_grid(bx,by,bz,Nx,Ny,Nz):
    """
    Parameters
    ----------
    bx : FLOAT
        Grid boundary in x direction (-xb to xb)
    by : FLOAT
        Grid boundary in y direction (-yb to yb).
    bz : FLOAT
        Grid boundary in z direction (0 to zb).
    Nx : INT
        Number of cells in x direction.
    Ny : INT
        Number of cells in y direction.
    Nz : INT
        Number of cells in z direction.

    Returns
    -------
    grid : DATA_COLLECTION
        Contains X, Y and Z meshgrids and various grid parameters.
    """
    grid = instance_of_data_collection()
    if Nx > 1:
        grid.dx = 2*bx/(Nx-1)
        x = np.linspace(-bx,bx,Nx)
    else:
        grid.dx = 1
        x = bx
    if Ny > 1:
        grid.dy = 2*by/(Ny-1)
        y = np.linspace(-by,by,Ny)
    else:
        grid.dy = 1
        y = by
    if Nz > 1:
        grid.dz = bz/(Nz-1)
        z = np.linspace(0,bz,Nz)
    else:
        grid.dz = 1
        z = bz
    grid.Nx = Nx
    grid.Ny = Ny
    grid.Nz = Nz
    grid.X,grid.Y,grid.Z = np.meshgrid(x,y,z)
    return grid

def wind_statistics(wd):
    """
    Parameters
    ----------
    wd : vector of wind directions
    
    Returns
    -------
    wd_avg : average of wd, accounting for cyclical nature of input
    wd_std : standard deviation of wd, accounting for cyclical nature of input
    """
    wd180  = wd[wd<=180]
    wd360  = wd[wd>180]
    
    N1      = len(wd180)
    N2      = len(wd360)
    A1      = np.sum(wd180)
    A2      = np.sum(wd360)
    S1      = np.sum(wd180**2)
    S2      = np.sum(wd360**2)
    
    if N2 == 0:
        wd_avg = A1/N1
        wd_std = np.sqrt((S1-(A1**2/N1))/(N1-1))
    elif N1 == 0:
        wd_avg = A2/N2
        wd_std = np.sqrt((S2-(A2**2/N2))/(N2-1))
    elif A2/N2 - A1/N1 < 180:
        wd_avg = (A1+A2)/(N1+N2)
        wd_std = np.sqrt((S1+S2-wd_avg**2*(N1+N2))/(N1+N2-1))
    elif A2/N2 - A1/N1 > 180:
        wd_avg = (A1+A2-N2*360)/(N1+N2)
        wd_std = np.sqrt((S1+S2-wd_avg**2*(N1+N2)+N2*360*(360-2*A2/N2))/(N1+N2-1))
        if wd_avg < 0:
            wd_avg = wd_avg + 360
    
    return wd_avg,wd_std

def WGS84_to_L72(Lat,Lng,x_ref=0,y_ref=0):
    """
    Parameters
    ----------
    Lat, Lng : Latitude / longitude in decimal degrees and in WGS84 datum 
    
    Returns
    -------
    x, y : Latitude / longitude in decimal degrees and in Belgian datum
    
    Reference
    ---------
    This algorithm is based on the algorithm supplied here:
    http://zoologie.umons.ac.be/tc/algorithms.aspx
    """
    
    # STEP ONE: CONVERT FROM WGS84 to Belgian Datum

    
    Haut = 0
    Lat = (np.pi/180)*Lat
    Lng = (np.pi/180)*Lng
     
    SinLat = np.sin(Lat)
    SinLng = np.sin(Lng)
    CoSinLat = np.cos(Lat)
    CoSinLng = np.cos(Lng)
    
    dx = 125.8
    dy = -79.9
    dz = 100.5
    da = 251.0
    df = 0.000014192702
     
    LWf = 1 / 297
    LWa = 6378388
    LWb = (1 - LWf) * LWa
    LWe2 = (2 * LWf) - (LWf * LWf)
    Adb = 1 / (1 - LWf)
     
    Rn = LWa / np.sqrt(1 - LWe2 * SinLat * SinLat)
    Rm = LWa * (1 - LWe2) / (1 - LWe2 * Lat * Lat) ** 1.5
     
    DLat = -dx * SinLat * CoSinLng - dy * SinLat * SinLng + dz * CoSinLat
    DLat = DLat + da * (Rn * LWe2 * SinLat * CoSinLat) / LWa
    DLat = DLat + df * (Rm * Adb + Rn / Adb) * SinLat * CoSinLat
    DLat = DLat / (Rm + Haut)
     
    DLng = (-dx * SinLng + dy * CoSinLng) / ((Rn + Haut) * CoSinLat)
    Dh = dx * CoSinLat * CoSinLng + dy * CoSinLat * SinLng + dz * SinLat
    Dh = Dh - da * LWa / Rn + df * Rn * Lat * Lat / Adb
     
    LatBel = ((Lat + DLat) * 180) / np.pi
    LngBel = ((Lng + DLng) * 180) / np.pi
    

    # STEP TWO: CONVERT FROM BELGIAN COORDINATES TO LAMBERT 72
    # Conversion from spherical coordinates to Lambert 72
    # Input parameters : Lat, Lng (spherical coordinates)
    # Spherical coordinates are in decimal degrees converted to Belgium datum!

    
    Lat = LatBel
    Lng = LngBel
    
    LongRef = 0.076042943        # =4°21'24"983
    bLamb = 6378388 * (1 - (1 / 297))
    aCarre = 6378388 ** 2
    eCarre = (aCarre - bLamb ** 2) / aCarre
    KLamb = 11565915.812935
    nLamb = 0.7716421928
    
    eLamb = np.sqrt(eCarre)
    eSur2 = eLamb / 2
    
    # conversion to radians
    Lat = (np.pi / 180) * Lat
    Lng = (np.pi / 180) * Lng
    
    eSinLatitude = eLamb * np.sin(Lat)
    TanZDemi = (np.tan((np.pi / 4) - (Lat / 2))) * \
       (((1 + (eSinLatitude)) / (1 - (eSinLatitude))) ** (eSur2))
    RLamb = KLamb * ((TanZDemi) ** nLamb)
    Teta = nLamb * (Lng - LongRef)
    
    x = 150000 + 0.01256 + RLamb * np.sin(Teta - 0.000142043)
    y = 5400000 + 88.4378 - RLamb * np.cos(Teta - 0.000142043)
    
    return x-x_ref,y-y_ref

def L72_to_WGS84(Xvec,Yvec):
    
    ## Step one: convert L72 to Belgian datum
    
    LatWGS84 = 0*Xvec
    LngWGS84 = 0*Yvec
    for i in range(Xvec.shape[0]):
        for j in range(Xvec.shape[1]):
            X=Xvec[i,j]
            Y=Yvec[i,j]
            LongRef = 0.076042943
            nLamb = 0.7716421928
            aCarre = 6378388**2
            bLamb = 6378388*(1-(1/297))
            eCarre = (aCarre-bLamb**2)/aCarre
            KLamb = 11565915.812935
         
            eLamb = np.sqrt(eCarre)
            eSur2 = eLamb / 2
         
            Tan1 = (X - 150000.01256) / (5400088.4378 - Y)
            Lambda = LongRef + (1 / nLamb) * (0.000142043 + np.arctan(Tan1))
            RLamb = np.sqrt((X - 150000.01256)**2 + (5400088.4378 - Y)**2)
         
            TanZDemi = (RLamb / KLamb)**(1 / nLamb)
            Lati1 = 2 * np.arctan(TanZDemi)
            
            Diff = 1e20
            
            while np.abs(Diff)>0.0000000277777:
                eSin = eLamb * np.sin(Lati1)
                Mult1 = 1 - eSin
                Mult2 = 1 + eSin
                Mult = (Mult1 / Mult2)**(eLamb / 2)
                LatiN = (np.pi / 2) - (2 * (np.arctan(TanZDemi * Mult)))
                Diff = LatiN - Lati1
                Lati1 = LatiN
         
            Lat = (LatiN * 180) / np.pi
            Lng = (Lambda * 180) / np.pi
            
            ## Step two: convert Belgian datum to WGS84
            
            Haut = 0
        
            Lat = (np.pi / 180) * Lat
            Lng = (np.pi / 180) * Lng
         
            SinLat = np.sin(Lat)
            SinLng = np.sin(Lng)
            CoSinLat = np.cos(Lat)
            CoSinLng = np.cos(Lng)
         
            dx = -125.8
            dy = 79.9
            dz = -100.5
            da = -251.0
            df = -0.000014192702
             
            LWf = 1 / 297
            LWa = 6378388
            LWb = (1 - LWf) * LWa
            LWe2 = (2 * LWf) - (LWf * LWf)
            Adb = 1 / (1 - LWf)
         
            Rn = LWa / np.sqrt(1 - LWe2 * SinLat * SinLat)
            Rm = LWa * (1 - LWe2) / (1 - LWe2 * Lat * Lat)**1.5
         
            DLat = -dx * SinLat * CoSinLng - dy * SinLat * SinLng + dz * CoSinLat
            DLat = DLat + da * (Rn * LWe2 * SinLat * CoSinLat) / LWa
            DLat = DLat + df * (Rm * Adb + Rn / Adb) * SinLat * CoSinLat
            DLat = DLat / (Rm + Haut)
         
            DLng = (-dx * SinLng + dy * CoSinLng) / ((Rn + Haut) * CoSinLat)
            Dh = dx * CoSinLat * CoSinLng + dy * CoSinLat * SinLng + dz * SinLat
            Dh = Dh - da * LWa / Rn + df * Rn * Lat * Lat / Adb
         
            LatWGS84[i,j] = ((Lat + DLat) * 180) / np.pi
            LngWGS84[i,j] = ((Lng + DLng) * 180) / np.pi
    
    return LatWGS84, LngWGS84

def yx_to_wd(x,y):
    wd = np.mod(-(np.arctan2(y,x)*180/np.pi-90-180),360)
    r = np.sqrt(x**2+y**2)
    return wd, r

def rotate_grid(X,Y,wd):
    """
    Parameters
    ----------
    X : Meshgrid of longitudinal coordinates (Lambert 72) w.r.t. stack [m]
    Y : Meshgrid of longitudinal coordinates (Lambert 72) w.r.t. stack [m]
    wd: Wind direction (antiparallel to wind vector) [deg. w.r.t. North]

    Returns
    -------
    Xrot,Yrot : X and Y components of a meshgrid rotated by (wd-90)/180*np.pi

    """
    r = np.sqrt(X**2+Y**2)
    phi = np.arctan2(Y,X) - np.pi + (wd-90)/180*np.pi
    Xrot = r*np.cos(phi)
    Yrot = r*np.sin(phi)
    
    return Xrot,Yrot