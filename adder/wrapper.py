# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 14:48:40 2023

@author: jfrankem
"""
include_pytensor = False

import numpy as np
from .mathematical_tools import WGS84_to_L72

from .gaussian_plume import multi_plume
from .gamma_dosimetry import gamma_factors

if include_pytensor:
    import pytensor.tensor as ptt

import multiprocessing as mp

        
class Meteo_var:
            
    def __init__(self):
        
        pass


class GaussianPlumeSimulation:
    """
    
    Description
    -----------
    This class was designed as a wrapper around the GPM calculations. By using
    this class, it is possible to interface with the GPM as an object which
    makes life a lot easier when doing optimisations etc.
    
    Methods
    ----------
    __init__ : initialises minimally required parameters
    run      : runs an GPM simulation given grid, meteo and source inputs

        
    """
    
    def __init__(self,switch_plume_rise='none',switch_plume_type='inversion',
                 L=1e20,Umin=0.5,verbose=False):
        """
        

        Parameters
        ----------
        switch_plume_rise : STRING, optional
            Turns plume_rise() functions on or off. The default is 'none'.
        switch_plume_type : STRING, optional
            Determines reflections. The default is 'inversion'.
        L : FLOAT, optional
            Determines height of ABL. Actual ABL height is the minimum between
            this value and the parameterised value. The default is 1e20.
        Umin : FLOAT, optional
            DESCRIPTION. The default is 0.5.
        verbose : BOOL, optional
            Determines whether the code gives updates during the run, The 
            default is False.

        Returns
        -------
        None.

        """
        
        self.switch_plume_rise = switch_plume_rise
        self.switch_plume_type = switch_plume_type
        self.L = L
        self.Umin = Umin
        self.verbose = verbose
        
    def run(self,grid,meteo,source):
        """
        

        Parameters
        ----------
        grid : SquareGrid type (cf. mathematical_tools.py).
            Meshgrid that contains at minimum X, Y and Z meshes.
        meteo : Meteo type (cf. read_inputs.py)
            Contains the required meteo data for the GPM to run.
        source : Source type (cf. ../example/selenium-75.py)
            Contains the required source parameters for the GPM.

        Returns
        -------
        c_run : list of np.arrays for each time step(cf. gaussian_plume.py)
            Concentration fields in time.

        """
        self.grid_run = grid
        self.meteo_run = meteo
        self.source_run = source
        self.c_run, self.TIC_run = multi_plume(grid, meteo, source, self.Umin, 
                                               self.switch_plume_type, 
                                               self.switch_plume_rise,
                                               verbose=self.verbose
                                               )
        return self.c_run
    
    def mprun(self,grid,meteo,source, ntasks=2, input_tuple=None):
        """
        

        Parameters
        ----------
        grid : SquareGrid type (cf. mathematical_tools.py).
            Meshgrid that contains at minimum X, Y and Z meshes.
        meteo : Meteo type (cf. read_inputs.py)
            Contains the required meteo data for the GPM to run.
        source : Source type (cf. ../example/selenium-75.py)
            Contains the required source parameters for the GPM.

        Returns
        -------
        c_run : list of np.arrays for each time step(cf. gaussian_plume.py)
            Concentration fields in time.

        """
        
        
        
        if input_tuple == None:
            N = len(meteo.U)
            
            grid_vars   = [grid]*ntasks
            Umin_vars   = [self.Umin]*ntasks
            spt_vars    = [self.switch_plume_type]*ntasks
            spr_vars    = [self.switch_plume_rise]*ntasks
            L_vars      = [1e20]*ntasks
            vb_vars     = [self.verbose]*ntasks
            
            dn = int(N/ntasks)
            dn_rem = int(N%ntasks)
            n1 = 0
            meteo_vars = []
            source_vars = []
            for i in range(ntasks):
                n0 = n1 
                if dn_rem > 0:
                    dn_rem -= 1
                    n1 = n1 + dn + 2
                else:
                    n1 = n1 + dn + 1
                mv          = Meteo_var()
                mv.t        = meteo.t[n0:n1]
                #mv.Tlow     = meteo.Tlow[n0:n1]
                #mv.Thigh     = meteo.Thigh[n0:n1]
                mv.height   = meteo.height
                mv.Ta       = meteo.Ta[n0:n1]
                mv.U        = meteo.U[n0:n1]
                mv.wd       = meteo.wd[n0:n1]
                #mv.sig_wd   = meteo.sig_wd[n0:n1]
                mv.E        = meteo.E[n0:n1]
                mv.T        = meteo.T
                meteo_vars.append(mv)
                
                sv          = Meteo_var()
                sv.Hs       = source.Hs
                sv.Ts       = source.Ts
                sv.Vs       = source.Vs
                sv.Q        = source.Q[n0:n1]
                source_vars.append(sv) 
            
        input_tuple = zip(grid_vars, meteo_vars, source_vars, Umin_vars,
                          spt_vars, spr_vars, L_vars, vb_vars)
        with mp.get_context("spawn").Pool(ntasks) as p:
            #p.starmap(multi_plume,input_tuple)
            results=p.starmap(multi_plume,input_tuple)
            
        return results
    
class DoseRateSimulation:
    """
    
    Description
    -----------
    This class was designed as a wrapper around the dose rate calculations. 
    By using this class, it is possible to interface with the DR as an object 
    which makes life a lot easier when doing optimisations etc.
    
    Methods
    ----------
    __init__ : initialises minimally required parameters, and calculates 
               gamma factor matrix for initialised detectors
    run      : calculates dose rates at all detectors given concentrations

        
    """
    def __init__(self,grid,detector_x,detector_y,detector_z,rho,
                 nuclide_data,database='nucl',dose_type='ambient',verbose=False):
        
        self.grid = grid
        self.detector_x = detector_x
        self.detector_y = detector_y
        self.detector_z = detector_z
        self.database = database
        self.rho = rho
        self.dose_type = dose_type
        
        if verbose: print('Calculating the gamma factor matrix. This may take some time.')
        
        if self.dose_type=='ambient':
            index = 0
        elif self.dose_type=='kerma':
            index = 1
        else:
            raise Exception('Inputted dose_type does not exist.')
        
        self.gf = []
        for i in range(len(self.detector_x[0])):
            if verbose: print('Calculating gamma factor matrix for detector '+str(i+1)+' of '+str(len(self.detector_x[0]))+'.')
            self.gf.append(gamma_factors(grid, detector_x[0][i],detector_y[0][i],detector_z, 
                                    nuclide_data, rho, database)[index])

    def run(self,c):

        Nd = len(self.detector_x[0])
        
        if c.ndim == 4:
            Nt = c.shape[3]
            dose = np.zeros([Nt,Nd])
            for i in range(Nt):
                for j in range(Nd):
                    dose[i,j]=np.sum(c[:,:,:,i]*self.gf[j])
        elif c.ndim == 3:
            for j in range(Nd):
                dose = np.zeros(Nd)
                dose[j]=np.sum(c*self.gf[j])
        else:
            raise Exception('c must either be a 3D spatial array or a 4D spatio-temporal array.')
        
        return dose
    
class TeleradSimulation:
    """
    
    Description
    -----------
    This class was designed as a wrapper around the dose rate calculations. It
    is the same as the DoseRateSimulation class except that it is constructed
    specifically for Telerad type data. Telerad type data can be obtained from
    the PyCMD Python Package.
    
    By using this class, it is possible to interface with the DR as an object 
    which makes life a lot easier when doing optimisations etc.
    
    Methods
    ----------
    __init__   : initialises minimally required parameters and calculates 
                 gamma factors using initialise() if auto=True
    initialise : calculates gamma factor matrix for initialised detectors
    run        : calculates dose rates at all detectors given concentrations

        
    """
    def __init__(self,grid,telerad,rho,
                 nuclide_data,database='nucl',stack='BR1',auto=False,verbose=False):
        
        self.grid = grid
        self.telerad = telerad # Accepts class Telerad
        self.Nd = len(self.telerad.detectors)
        self.rho = rho
        self.nuclide_data = nuclide_data
        self.database = database
        self.gamma_factors = []
        self.verbose = verbose
        if stack == 'BR1':
            self.x_ref = 199985 #BR1 x L72
            self.y_ref = 212125 #BR1 y L72
        else:
            raise Exception('Stack not known.')
        
        if auto:
            self.initialise()

    def initialise(self):
        if self.verbose: print('Calculating the gamma factor matrix. '
                               +'This may take some time.')
        for i,d in enumerate(self.telerad.detectors):
            if self.verbose: 
                print('Calculating gamma factor matrix for '
                      +d.name+ ' (' + str(i+1) + ' of ' 
                      +str(len(self.telerad.detectors))+ ').')
            x, y = WGS84_to_L72(d.lat, d.lon, self.x_ref, self.y_ref)
            z = 1
            self.gamma_factors.append(
                gamma_factors(self.grid,x,y,z,self.nuclide_data, 
                              self.rho, self.database)[0])
            
    def run(self,c):
        dose_rate = []
        for i,d in enumerate(self.telerad.detectors):
            if isinstance(c, list):
                if len(c) == 1:
                    dose_rate.append(ptt.sum(c[0]*self.gamma_factors[i]))
                else:
                    Nt = len(c)
                    dose_rate_i = []
                    for j in range(Nt):
                        if include_pytensor:
                            dose_rate_i.append(ptt.sum(c[j]*self.gamma_factors[i]))
                        else:
                            dose_rate_i.append(np.sum(c[j]*self.gamma_factors[i]))
                    dose_rate.append(dose_rate_i)
            elif c.ndim == 4: #the fourth dimension is time
                Nt = len(d.val)
                dose_rate_i = np.zeros(Nt)
                for j in range(Nt):
                    dose_rate_i[j] = np.sum(c[:,:,:,j]*self.gamma_factors[i])
                dose_rate.append(dose_rate_i)
            else:
                dose_rate.append(np.sum(c*self.gamma_factors[i]))
        return dose_rate