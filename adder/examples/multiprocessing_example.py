# -*- coding: utf-8 -*-
"""
Created on Fri May 17 09:02:51 2024

@author: jfrankem
"""

import numpy as np

from adder_lib.wrapper import GaussianPlumeSimulation
from adder_lib.mathematical_tools import SquareGrid
from adder_lib.read_inputs import read_selenium_meteo
import time


# We retrieve the meteorological data
t0              = np.datetime64('2019-05-15 00:00')
t1              = np.datetime64('2019-05-15 23:59')
meteo           = read_selenium_meteo(t0, t1)

# We define the source term
class Source():
    
    def __init__(self):
        
        self.Hs = 60 # height of the BR2 stack [m]
        self.Ts = 15 # temperature of exhausted gas [deg. Celsius]
        self.Vs = 150000/3600 # gas outflow rate [m3/s]
        self.Q = np.tile(57e9/3600,len(meteo.U)) # source term [Bq/s]

source = Source()

# We initialise the grid and simulation
grid = SquareGrid(bx=1000, by=1000, bz=200, Nx=101, Ny=101, Nz=21)
gps = GaussianPlumeSimulation(verbose=False)


if __name__ == '__main__':
    
    print('Starting parallel task')
    t1 = time.time()
    results=gps.mprun(grid, meteo, source,ntasks=4)
       
    t2 = time.time()
    print('Starting sequential task')
    
    c2 = gps.run(grid, meteo, source)
    t3 = time.time()
    
    print('Parallel execution took '+str(t2-t1)+' seconds, while sequential execution took '+str(t3-t2))

 