# -*- coding: utf-8 -*-
"""
THIS SCRIPT CURRENTLY ONLY RUNS IN VERSION1.0 OF ADDER
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates 

from adder.wrapper import GaussianPlumeSimulation, DoseRateSimulation
#from adder.gamma_dosimetry import CloudShine, PointSourceDose
from adder.gamma_dosimetry import CloudShine, PointSourceDose
from adder.read_inputs import read_selenium_meteo, read_livechart_api, Source
from adder.mathematical_tools import SquareGrid

class CloudShine(PointSourceDose):
    
    def calculate_cloud_shine(self,grid,x,y,z,c,rho=0.001205,r_max=1000,num=15,alg='pchip'):
        self.interp_ps_profile(rho,r_max,num,1,alg)
        R = np.sqrt((grid.X-x)**2+(grid.Y-y)**2+(grid.Z-z)**2)
        return grid.dx*grid.dy*grid.dz*np.nansum(c*np.exp(self.interp_gf(R)))

"""
1. Set-up of the computation
"""

# We set up the numerical grid
Nx, Ny, Nz = 101, 101, 21
bx, by, bz = 500, 500, 200
grid = SquareGrid(bx,by,bz,Nx,Ny,Nz)

# We retrieve the meteorological data
t0              = np.datetime64('2019-05-15 15:20')
t1              = np.datetime64('2019-05-15 16:10')
meteo           = read_selenium_meteo(t0, t1)

# We set up the source term. If you are working with larger datasets,
# it might be advisable to instead set up an automatic data import function 
# like the example written for the meteorological data below. This function
# could go into read_inputs.py.
Hs = 60. # height of the BR2 stack [m]
Ts = 15. # temperature of exhausted gas [deg. Celsius]
Vs = 150000/3600 # gas outflow rate [m3/s]
Q = np.array([8.3,8.3,8.3,0.,0.,0.])*1e6 # source term [Bq/s]
t = meteo.t

source = Source(Q, t, Hs, Vs, Ts)



# We set some options for the dispersion simulation and initialise the plume class
switch_plume_type = 'inversion' # include ground and inversion reflection
switch_plume_rise = 'hx' # plume rise varies as function of downwind distance
L = 1e20 # constrain the dispersion by the mixing height of the atmosphere
Umin = 0.5 # lower limit of the wind speed [m/s]

gps = GaussianPlumeSimulation(switch_plume_rise,switch_plume_type,L,Umin,verbose=False)

# We set up the locations of the dose rate stations.
xq = np.array([-157.2,-262.2,-305.8])
yq = np.array([-142.8,-121.6,  44.9])
zq = np.array([   1.0,   1.0,   1.0])

# We set some options for the gamma dose rate calculations
#nuclide_data = read_livechart_api('Se-75')
nuclide_vector = {}
nuclide_vector['75Se']=1
database = 'nucl' # We use the ANS (1991) build-up factors because they go to lower keV than Martin (2013)
rho = 0.0012 # air density [g/cm3]
dose_rate_type = 0 # set to 0 for ambient dose equivalent rate [nSv/h] or to 1 for air kerma [nGy/h]

#drs = DoseRateSimulation(grid, xq, yq, zq, rho, nuclide_data)

cs = CloudShine(nuclide_vector,E_c=50,I_c=0.01)

# To compare with measurements, we add some real data below. Observations are
# from TELERAD (courtesy of FANC-ACFN). Backgrounds are subtracted.
H10T = np.transpose(np.array([   
    [ 3.8684,    3.7368,     0.4053],
    [ 2.0684,    2.2368,     1.8053],
    [ 1.5684,    2.5368,    -0.0947],
    [ 1.1684,    0.5368,     0.4053],
    [-0.2316,    0.3368,     0.4053],
    [ 0.9684,    0.7368,    -0.1947]]))
sig2H10T = np.array([ 1.0542,	0.9457,	    0.8013])

"""
2. Actual computation
"""

# Calculate concentration fields in time via a Gaussian plume model
c = gps.run(grid, meteo, source)
# set 'infinite value' at stack to zero
for i in range(len(c)):
    c[i][50,50,6]=0
plt.contourf(np.squeeze(grid.X[:,:,0]),
             np.squeeze(grid.Y[:,:,0]),
             np.squeeze(gps.TIC_run[:,:,0])
             )
cbar=plt.colorbar()
cbar.ax.get_yaxis().labelpad = 15
cbar.ax.set_ylabel('TIC [Bq s m$^{-3}$]', rotation=270)
# Calculate the dose rate in time for each detector
dr = []
for x, y, z in zip(xq,yq,zq):
    dri = []
    for ci in c:
        dri.append(cs.calculate_cloud_shine(grid, x, y, z, ci))
    dr.append(dri)
dr=np.array(dr)
"""
3. Visualisation
"""

fig = plt.figure()
gs = fig.add_gridspec(1,3)
ax1,ax2,ax3 = gs.subplots(sharex=True, sharey=True)

ax1.errorbar(meteo.t,H10T[0,:], yerr=sig2H10T[0],capsize=5)
ax2.errorbar(meteo.t,H10T[1,:], yerr=sig2H10T[1],capsize=5)
ax3.errorbar(meteo.t,H10T[2,:], yerr=sig2H10T[2],capsize=5)
ax1.plot(meteo.t,dr[0,:])
ax2.plot(meteo.t,dr[1,:])
ax3.plot(meteo.t,dr[2,:])

date_format = mdates.DateFormatter('%H:%M')

ax1.set_ylim([-2, 6])
ax1.tick_params(labelrotation=45)
ax2.tick_params(labelrotation=45)
ax3.tick_params(labelrotation=45)
ax1.xaxis.set_major_formatter(date_format)
ax2.xaxis.set_major_formatter(date_format)
ax3.xaxis.set_major_formatter(date_format)
ax1.set_box_aspect(1)
ax2.set_box_aspect(1)
ax3.set_box_aspect(1)
ax1.tick_params(direction='in',right=True,top=True)
ax2.tick_params(direction='in',right=True,top=True)
ax3.tick_params(direction='in',right=True,top=True)
ax1.set_title('IMR/M03')
ax2.set_title('IMR/M15')
ax3.set_title('IMR/M04')
ax1.set_ylabel('nSv/h')
plt.legend(['GPM','TELERAD'])
