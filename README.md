## ADDER
Atmospheric Dispersion and Dose Equivalent Rates (ADDER) is a near-range atmospheric dispersion model including dose rate model.

## Description
ADDER uses meteorological and source term data to calculate ambient dose equivalent rates (nSv/h) due to the near-range atmospheric dispersion of radionuclides. It has been  developed as part of a PhD project on near-range atmospheric dispersion for use in impact studies. It was used to analyse a anomalous emission of selenium-75 in Belgium, 2019 (Frankemölle et al, 2022a) as well as for several routine emissions (Frankemölle et al, 2022b).

## Installation
ADDER was developed in miniconda3. Besides some packages included in the standard Python distribution, it depends on numpy and pandas. Additionally, in case it is used in concert with PyMC, it requires PyTensor. If this import gives trouble, it can safely be commented out in wrapper.py (both the import and use in TeleradSimulation).

## References
Frankemölle, J.P.K.W. Camps, J., De Meutter, P., Antoine, P., Delcloo, A.W., Vermeersch, F. & Meyers, J. (2022a) 'Near-range atmospheric dispersion of an anomalous selenium-75 emission', _Journal of Environmental Radioactivity_, 255, pp.107012. DOI: https://doi.org/10.1016/j.jenvrad.2022.107012

Frankemölle, J.P.K.W., Camps, J., De Meutter, P. & Meyers, J. (2022b) 'Near-range Gaussian plume modelling for gamma dose rate reconstruction', _21st International Conference on harmonisation within Atmospheric Dispersion Modelling for Regulartory Purposes, 27–30 September 2022, Aveiro, Portugal_.
