from setuptools import setup, find_packages

setup(
    name='ADDER',
    version='1.0.3',
    url='https://gitlab.com/jpfr95/adder.git',
    author='J.P.K.W. Frankemölle',
    author_email='jens.peter.frankemolle@sckcen.be',
    description='A Gaussian plume and finite cloud dose model',
    packages=find_packages(include=['adder','adder.*']),
    install_requires=[
        'numpy',
        'scipy',
        'pandas']
)
